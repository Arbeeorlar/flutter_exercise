import 'package:flutter/material.dart';


class NavigationHeader extends StatelessWidget {
  final Function onClick;

  NavigationHeader({this.onClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        right: 10.0,
      ),
      margin: EdgeInsets.only(top: 50.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 20),
            child: IconButton(
              icon: new Image.asset(
                "images/arrow_back.png",
                width: 55,
                height: 45,
              ),
              //Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),

        ],
      ),
    );
  }
}
