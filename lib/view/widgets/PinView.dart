import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



class PinView extends StatefulWidget {
  final int boxes;
  final ValueChanged<String> onPinComplete;
  final ValueChanged<String> onPinEntered;
  final bool focusOnEnter;
  final bool obscure;
  final EdgeInsets margin;
  // final String initialValue;
  final TextEditingController controller;


  PinView({
    this.boxes = 4,
    this.onPinComplete,
    this.onPinEntered,
    this.focusOnEnter = true,
    this.obscure = false,
    this.controller,
    this.margin = const EdgeInsets.symmetric(horizontal: 10.0),
  });

  @override
  State<StatefulWidget> createState() => _PinViewState();
}

class _PinViewState extends State<PinView> {

  // TextEditingController mainEditingController = new TextEditingController();

  List<TextEditingController> textEditingControllers = [];
  List<FocusNode> focusNodes = [];

  List<String> pin = [];

  int currentFocusBoxIndex = 0;

  int selectedColorFocusIndex = 0;

  @override
  void initState() {
    super.initState();

    // pin = List(widget.boxes);

    for (int i = 0; i < widget.boxes; i++) {
      focusNodes.add(FocusNode());
      textEditingControllers.add(TextEditingController());
      focusNodes[i].addListener(onPinBoxFocused);
    }

    if (widget.controller != null) {
      widget.controller.addListener(() {
        print("Pin Changes");
        if (widget.controller.text.isEmpty) {
          clearAll();
        }
      });
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (widget.focusOnEnter) {
        FocusScope.of(context).requestFocus(focusNodes[0]);
      }
    });

    /*widget.controller.addListener(() {
      print("Perform a clear... for PIN");
    });*/

  }

  void clearAll() {
    textEditingControllers.forEach((ctrler) {
      ctrler.clear();
    });
    pin.clear();
    selectedColorFocusIndex = 0;
    FocusScope.of(context).requestFocus(focusNodes[0]); // Request Focus on the first on
    setState(() {});
  }

  //
  onPinBoxFocused() {
    print("One of the boxes has been focused on..");
    int boxFocusIndex = 0;
    for (int i = 0; i < focusNodes.length; i++) {
      if (focusNodes[i].hasFocus) {
        print("The Correct focus node is: $i");
        boxFocusIndex = i;
      }
    }
    if (boxFocusIndex > 0) {
      currentFocusBoxIndex = pin.isEmpty ? 0 : (pin.length - 1);
      print("Request focus at: $currentFocusBoxIndex");
      // send the focus back to the last one
      FocusScope.of(context).requestFocus(focusNodes[currentFocusBoxIndex]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(widget.boxes, (index) {
        return Expanded(child: pinBox(index));
      }),
    );
  }

  Widget pinBox(int index) {
    return Container(
      padding: EdgeInsets.all(16.0),
      margin: widget.margin,
      width: 68,
      height: 64,
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: BorderRadius.circular(20.0),
        border: selectedColorFocusIndex == index
            ? Border.all(color: Theme.of(context).primaryColor.withOpacity(0.3))
            : Border(),
      ),
      child: Container(
        alignment: Alignment.center,
        height: 24,
        child: TextFormField(
          key: ValueKey('$index'),
          obscureText: widget.obscure,
          controller: textEditingControllers[index],
          focusNode: focusNodes[index],
          // enabled: index == 0 ? true : false,
          showCursor: false,
          enableInteractiveSelection: false,
          /*onTap: () {
            print("Index Tapped at..... $index");
            if (index > 0) {
              int focusPoint = pin.isEmpty ? 0 : (pin.length - 1);
              print("Request focus at: $focusPoint");
              // send the focus back to the last one
              FocusScope.of(context).requestFocus(focusNodes[focusPoint]);
            }
          },*/
          decoration: InputDecoration(
            filled: true,
            hintText: "\u002D",
            contentPadding: EdgeInsets.only(bottom: 10, top: 0),
            border: InputBorder.none,
            // focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
          ),

          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
          inputFormatters: [
            PinTextInputFormatter(1, (newValue) {
              // print("Future Value is: $newValue");
              this.jumpToNextPinBox(newValue);
            }),
          ],
//          inputFormatters: [PinTextInputFormatter(), LengthLimitingTextInputFormatter(1)],
          keyboardType:
          TextInputType.numberWithOptions(signed: false, decimal: false),
          onChanged: (val) {
            // print("Onchanged Val - $val");
            // Note: The onchange can only be called

            // print("Onchanged(): The Pin Before: $pin");

            // The below is for widget text
            // this.jumpToNextPinBox(val); // comment after widget testing
            if (val.isNotEmpty) {
              if (pin.isEmpty) {
                pin.add(val);
              }
            } else {
              pin.removeLast();
              if (pin.isNotEmpty) focusOn(pin.length - 1);
              // pin.removeAt(index);
            }
            // print("Onchanged(): Pin After: $pin");

            /*if (val.isNotEmpty) {

              if (pin.length < widget.boxes) { pin.add(val); }

              if (pin.length >= widget.boxes) {
                if (widget.onPinComplete != null) {
                  widget.onPinComplete(pin.join());
                }
              }

              if (index < (focusNodes.length - 1) ) {
                FocusScope.of(context).requestFocus(focusNodes[index + 1]);
              }
              print("------------ \n");

            } else {
              pin.removeAt(index); // [index] = "";
              focusOnPrevious(index);
            }
            print("Pin Values - $val & Length + $pin");*/
          },
        ),
      ),
    );
  }

  void jumpToNextPinBox(String value) {
    var index = (pin.length);
    var lastEntry = value[value.length - 1];
    // print("Last Entry: $lastEntry");

    if (pin.length < widget.boxes) {
      pin.add(lastEntry);
      textEditingControllers[index].text = lastEntry;
      FocusScope.of(context).requestFocus(focusNodes[index]);
      // print("Pin COntents: $pin");

      // set the index
      selectedColorFocusIndex = index;

      // Check for the end here
      if (pin.length >= widget.boxes) {
        if (widget.onPinComplete != null) {
          // print("COMPLETE PIN --- current focus : $currentFocusBoxIndex");

          // Bug Alert:(Lekan) - the keyboard not auto closing while navigating off the screen
          // this will help force the pin boxes to loose focus and drop the keyboard down
          // focusNodes[currentFocusBoxIndex].unfocus();
          focusNodes[currentFocusBoxIndex + 1].unfocus();
          FocusScope.of(context).unfocus();

          widget.onPinComplete(pin.join());
        }
      }
    }
    // ----------------
    setState(() {});
  }

  void focusOn(int position) {
    FocusScope.of(context).requestFocus(focusNodes[position]);
    selectedColorFocusIndex = position;
    setState(() {});
  }
}

class PinTextInputFormatter extends TextInputFormatter {
  PinTextInputFormatter(this.maxLength, this.newCharacterEntered)
      : assert(maxLength == null || maxLength == -1 || maxLength > 0);

  final int maxLength;
  final Function(String value) newCharacterEntered;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    // var numPattern = RegExp(r"[0-9]");
    var numericNoSymbolsRegExp = RegExp(r"^[0-9]+$");
    var emptyString = RegExp(r"^\s*$");

    if (oldValue.text.isEmpty && newValue.text.trim().isEmpty) {
      return oldValue;
    }

    if (numericNoSymbolsRegExp.hasMatch(newValue.text) ||
        emptyString.hasMatch(newValue.text)) {
      return limiting(oldValue, newValue);
      // return newValue;
    }
    // else its not a number
    return oldValue;
  }

  TextEditingValue limiting(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (maxLength != null &&
        maxLength > 0 &&
        newValue.text.runes.length > maxLength) {
      this.newCharacterEntered(
          newValue.text); // notify use that a new character has been entered

      final TextSelection newSelection = newValue.selection.copyWith(
        baseOffset: math.min(newValue.selection.start, maxLength),
        extentOffset: math.min(newValue.selection.end, maxLength),
      );
      // This does not count grapheme clusters (i.e. characters visible to the user),
      // it counts Unicode runes, which leaves out a number of useful possible
      // characters (like many emoji), so this will be inaccurate in the
      // presence of those characters. The Dart lang bug
      // https://github.com/dart-lang/sdk/issues/28404 has been filed to
      // address this in Dart.
      // TODO(gspencer): convert this to count actual characters when Dart
      // supports that.
      final RuneIterator iterator = RuneIterator(newValue.text);
      if (iterator.moveNext())
        for (int count = 0; count < maxLength; ++count)
          if (!iterator.moveNext()) break;
      final String truncated = newValue.text.substring(0, iterator.rawIndex);
      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}
