import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';

class PoDateField extends StatefulWidget {
  final DateTime minDateTime;
  final DateTime maxDateTime;
  final DateTime initialDateTime;
  final DateTime selectedDate;
  final String title;
  final ValueChanged<DateTime> onDateSelected;
  final bool error;

  // final Function(String, bool) error;

  PoDateField(this.minDateTime, this.maxDateTime, this.initialDateTime,
      {this.title,
      this.selectedDate,
      this.onDateSelected,
      this.error = false,
      Key key})
      : super(key: key);

  @override
  _PoDateFieldState createState() => _PoDateFieldState();
}

class _PoDateFieldState extends State<PoDateField> {
  final ValueNotifier<DateTime> dateNotifier =
      ValueNotifier(null);
  TextEditingController dateOfBirthController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    dateNotifier.value = this.widget.selectedDate;

    return ValueListenableBuilder(
        valueListenable: dateNotifier,
        builder: (context, DateTime value, child) {
          return Container(
            width: double.infinity,
            child: FlatButton(
              key: Key('dob'),
              color: Colors.grey.shade200,
              onPressed: () {
                showDatePicker(context);
              },
              child: value == null
                  ? Text(widget.title ?? "Select Date")
                  : Text(
                      '${value.year}-${value.month.toString().padLeft(2, '0')}-${value.day.toString().padLeft(2, '0')}',
                      style: Theme.of(context).textTheme.body1,
                    ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                side: this.widget.error
                    ? BorderSide(color: Colors.red)
                    : BorderSide.none,
              ),
              padding: EdgeInsets.symmetric(vertical: 18.0),
            ),
          );
        });
  }

  Widget build4(BuildContext context) {
    dateNotifier.value = this.widget.selectedDate;
    return ValueListenableBuilder(
        valueListenable: dateNotifier,
        builder: (context, DateTime value, child) {
          dateOfBirthController.text =  '${value.year}-${value.month.toString().padLeft(2, '0')}-${value.day.toString().padLeft(2, '0')}';
          return Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: TextFormField(
              controller: dateOfBirthController,
              showCursor: true,
              enabled: false,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              autocorrect: false,
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                prefixIcon: InkWell(
                  onTap: (){
                    showDatePicker(context);
                  },
                    child: Icon(Icons.calendar_today, color: AppColors.black)),
                // set the prefix icon constraints
                prefixIconConstraints: BoxConstraints(
                  minWidth: 25,
                  minHeight: 25,
                ),
                enabledBorder: new UnderlineInputBorder(
                    borderSide: new BorderSide(color: AppColors.black)),
                labelText: "Select Date",
                labelStyle: TextStyle(fontSize: 10.0, color: AppColors.black),
              ),
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                  color: AppColors.white),
              inputFormatters: [LengthLimitingTextInputFormatter(50)],
              onChanged: (text) {},
              validator: (String arg) {
                if (arg.length < 3)
                  return 'Fullname  is required';
                else
                  return null;
              },
              onSaved: (String val) {},
            ),
          );
        });
  }

  void border() {}

  void showDatePicker(BuildContext context) {
    DatePicker.showDatePicker(context,
        pickerTheme:
            DateTimePickerTheme(confirm: Text("Done"), cancel: Text("Close")),
        maxDateTime: widget.maxDateTime,
        // DateTime.parse(previous18Years),
        minDateTime: widget.minDateTime,
        initialDateTime: this.widget.initialDateTime ?? DateTime.now(),
        onConfirm: (dateTime, List<int> index) {
      this.dateNotifier.value = dateTime;
      if (this.widget.onDateSelected != null) this.widget.onDateSelected(dateTime);
    });
  }
}
