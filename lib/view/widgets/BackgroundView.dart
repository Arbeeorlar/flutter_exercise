

import 'package:flutter/material.dart';

class BackgroundView extends StatelessWidget {
  final String imageAsset;
  final double opacity;

  BackgroundView({this.imageAsset = "images/background.png", this.opacity = 0.5});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(this.imageAsset),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}