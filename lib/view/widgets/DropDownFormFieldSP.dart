
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';

class DropDownFormFieldSP<T> extends StatefulWidget {
  final List<T> items;
  final String hint;
  final Function(dynamic) onItemSelected;
  final Color backgroundColor;

  const DropDownFormFieldSP(
      {Key key, @required this.items, @required this.hint, @required this.onItemSelected, this.backgroundColor})
      : super(key: key);

  @override
  _DropDownFormFieldSPState createState() => _DropDownFormFieldSPState();
}

class _DropDownFormFieldSPState<T> extends State<DropDownFormFieldSP> {
  String _selectedOptionText;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 50,
          decoration: BoxDecoration(
            color: widget.backgroundColor ?? Colors.grey[200],
            borderRadius: BorderRadius.circular(90),
            border: Border.all(
              color: Colors.grey[200],
            ),
          ),
          child: Center(
            child: Text(_selectedOptionText ?? widget.hint,
                style: TextStyle(fontSize: 16, color: AppColors.black)),
          ),
        ),
        Positioned(
            right: 10,
            top: 10,
            child: Icon(
              Icons.arrow_drop_down,
              size: 30,
              color: AppColors.black,
            )),
        Container(
          height: 50,
          child: DropdownButton<T>(
            items: widget.items.map((Object value) {
              return new DropdownMenuItem<T>(
                value: value,
                child: Text(
                  value.toString(),
                  style: TextStyle(color: Colors.black),
                ),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                _selectedOptionText = value.toString();
              });
              widget.onItemSelected(value);
            },
            underline: Container(),
            isExpanded: true,
            dropdownColor: Colors.white,
            isDense: true,
            hint: Container(
              child: Center(
                child: Text(
                  '',
                ),
              ),
            ),
            icon: Container(),
            onTap: () {
              print('Item Selected');
            },
          ),
        ),

      ],
    );
  }
}