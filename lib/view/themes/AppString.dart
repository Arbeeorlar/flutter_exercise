class AppString {

 static String walkThroughText_11 = "go beyond";
 static String walkThroughText_12 = "the digital experience";

 static String walkThroughText_13 = "We Recognise the Role of Customer in our Branches.";
 static String walkThroughText_14 = "We are Here to Serve You.";
 static const String boardHeader1 = "Manage your property";
 static const String boardsub1 = 'Organize and manage your properties and units, manage occupancy.';
 static const String boardHeader2 = "Find your perfect home";
 static const String boardsub2 = 'With over 700,000 active listings property, connect to agent and property owner.';
 static const String boardHeader3 = "Secure Online Payment";
 static const String boardsub3 = 'Secure platform for tenants and residents to pay rent, service charge, estate due.';
 static const String timeoutException = 'Connection Timeout, Please try again';
 static const String noInternet = 'No internet connection';
 static const String badposturl = "Couldn't find the post 😱";
 static const String badrequest = 'Server unavailable, Please try again later'; 
 static const String success = "1";
 static const String failed = "0";
 static const String dummyAuthorizationToken = "";
}