import 'package:flutter/material.dart';

class AppColors {
  static Color primary = Color(0xFF2E1963);
  static Color primaryLight = Color(0xFF7362A1);
  static Color buttonLight =  Color(0xFFFFC175);


  static Color formError  =  Colors.red;
  static Color transparent =  Colors.transparent;
  static Color white = Colors.white;
  static Color black  =  Colors.black;
  static Color grey  =  Color(0xFFA7A7A7);
  static Color titleGreen  =  Color(0xFF4C9F43);
   static Color selectedColor  =  Color(0xFFff6d00);
   static Color greyLight = Color(0xFFD4D4D4);
   static Color lighter = Color(0xFF4BB74A8F);
  static Color greenligher = Color(0xFF43B842); //
  static Color yellowligher = Color(0xFFEEC600);
  static Color g = Color(0xFFffcdd2); //#F6FAFF
  static Color segmentColor = Color(0xFFE68639);
}