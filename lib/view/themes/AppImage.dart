class AppImage{
  static const String longLogo = "images/R2.png";
  static const String house = "images/Houses-bro.png";
    static const String commercialImage = "images/architecture-and-city.png";
    static const String residentialImage = "images/neighborhood.png";
    static const String emailGif = "images/4541-mail-verification.gif";
    static const String room = "images/room.jpg";
   static const String bedroom = "images/bathroom.png";
  static const String bed = "images/bed.png";
  static const String profile = "images/dp.png";
  static const String board1 = "images/respay_illus1.png";
  static const String board2 = "images/respay_illus2.png";
  static const String board3 = "images/respay_illus3.png"; //
  static const String userIcon = "images/usericon.png"; 
  static const String message = "images/respay_msg.png"; 
  static const String imageloader = "images/loader.gif";
  static const String defaulthouse = "images/house.jpg";
  static const String successGIF = "images/lottie/5449-success-tick.json";
//  static const String imageloader = "images/lottie/23181-yellow-house-with-a-green-roof.json";
 // static const String house2GIF = "images/lottie/27516-house-animations.json";//
  static const String addPropertySuccess = "images/lottie/6951-success.json"; //
    static const String deleteGif = "images/lottie/15120-delete.json"; //
      static const String faileGIF = "images/lottie/4185-error-circle-to-triangle-shape-morph.json";//images/lottie/12821-failed-attempt.json
  static const String confirmGIF = "images/lottie/5707-error.json";
  static const String cancel_faileGIF = "images/lottie/6952-fail.json";

}