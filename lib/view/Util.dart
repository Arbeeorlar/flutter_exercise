
import 'package:flutter/material.dart';

class Utilities {

  static bool validateEmail(String email) {
    if (RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email)) {
      return true;
    } else {
      return false;
    }
  }

 static showSnackBar(GlobalKey<ScaffoldState> scaffoldKey, {String message}) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message ?? ""),
      backgroundColor: Colors.orange,
    ));
  }

}

class Constant{

  static get  stateUrl  => "http://locationsng-api.herokuapp.com/api/v1/states" ;
  static get city =>"http://locationsng-api.herokuapp.com/docs/index.html#get-cities-in-a-state";

}