
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';
import 'SignUp/mobileregistration/SignUpScreen.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
     loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), moveToSignUp);
  }

  void moveToSignUp() async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => SignUpScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
              backgroundColor: AppColors.transparent,
              body:  Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.5), BlendMode.dstATop),
                    image: AssetImage(
                      "images/background.png",
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        right: 13, left: 13,),
                      alignment: Alignment.center,
                      child:  Image(image: AssetImage('images/flutter.jpeg'),width: 40,height: 40,),
                    ),
                    SizedBox(height: 20),
                    Container(
                      alignment: Alignment.center,
                      child: Text("Welcome...",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              color: AppColors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Source Sans Pro')),
                    ),




                  ],
                ),
              )
          )
      ),

    );
  }
}




