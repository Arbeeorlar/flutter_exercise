

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';
import 'package:flutter_exercise/view/widgets/BackgroundView.dart';
import 'package:flutter_exercise/view/widgets/DropDownFormFieldSP.dart';

import '../../../Util.dart';

class SecurityQuestionScreen extends StatefulWidget {
  @override
  _SecurityQuestionScreenState createState() => _SecurityQuestionScreenState();
}

class _SecurityQuestionScreenState extends State<SecurityQuestionScreen> {

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController firstAnswerController = TextEditingController();
  TextEditingController secondAnswerController = TextEditingController();

  List<String> securityQuestions = ["Who is your favourite author?","Who was your first boss?"];
  String firstQuestion ;
  String secondQuestion ;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        BackgroundView(),
        SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            key: scaffoldKey,
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: Container(
              child: Form(
                key: formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30,),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 30,right: 30),
                            decoration: BoxDecoration(
                              color: AppColors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                SizedBox(height: 30.0),
                                Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(left: 16, right: 30),
                                  child: Text(
                                    "Security Questions",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.black),
                                  ),
                                ),

                                SizedBox(height: 50.0),
                                Container(
                                  margin : EdgeInsets.only(left: 30, right: 30),
                                  child: DropDownFormFieldSP<String>(
                                    hint: 'Select Security Question',
                                    items: securityQuestions,
                                    onItemSelected: (value) {
                                      setState(() {
                                        firstQuestion = value ;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 30, right: 30),
                                  child: TextFormField(
                                    controller: firstAnswerController,
                                    showCursor: true,
                                    keyboardType:
                                    TextInputType.text,
                                    maxLength: 100,
                                    maxLines: 5,
                                    textInputAction: TextInputAction.next,
                                    autocorrect: false,
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                      enabledBorder: new UnderlineInputBorder(
                                          borderSide: new BorderSide(color: AppColors.black)
                                      ),
                                      labelText: "Input OTP",
                                      labelStyle: TextStyle(
                                          fontSize: 10.0,
                                          color: AppColors.black),
                                    ),
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14.0,
                                        color: AppColors.black),
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(50)
                                    ],
                                    onChanged: (text) {

                                    },
                                    validator: (String arg) {
                                      if (arg.length < 6)
                                        return 'Invalid OTP';
                                      else
                                        return null;
                                    },
                                    onSaved: (String val) {},
                                  ),
                                ),
                                SizedBox(height: 30.0),
                                Container(
                                  margin : EdgeInsets.only(left: 30, right: 30),
                                  child: DropDownFormFieldSP<String>(
                                    hint: 'Select Security Question',
                                    items: securityQuestions,
                                    onItemSelected: (value) {
                                      if(firstQuestion == secondQuestion){

                                      }
                                      setState(() {
                                        secondQuestion = value ;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 30, right: 30),
                                  child: TextFormField(
                                    controller: secondAnswerController,
                                    showCursor: true,
                                    keyboardType:
                                    TextInputType.text,
                                    maxLength: 100,
                                    maxLines: 5,
                                    textInputAction: TextInputAction.next,
                                    autocorrect: false,
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                      enabledBorder: new UnderlineInputBorder(
                                          borderSide: new BorderSide(color: AppColors.black)
                                      ),
                                      labelText: "Input OTP",
                                      labelStyle: TextStyle(
                                          fontSize: 10.0,
                                          color: AppColors.black),
                                    ),
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14.0,
                                        color: AppColors.black),
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(50)
                                    ],
                                    onChanged: (text) {

                                    },
                                    validator: (String arg) {
                                      if (arg.length < 6)
                                        return 'Invalid OTP';
                                      else
                                        return null;
                                    },
                                    onSaved: (String val) {},
                                  ),
                                ),
                                SizedBox(height: 30.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 45, right: 45),
                                  width: double.infinity,
                                  height: 50,
                                  child: FlatButton(
                                    onPressed: validate,
                                    child:  Text(
                                      "Continue",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    color: AppColors.primaryLight,
                                    textColor: AppColors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20.0),

                                SizedBox(height: 35.0),

                              ],
                            ),
                          ),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }


  void validate() {
    final FormState form = formKey.currentState;
    if (form.validate()) {

    } else {
      Utilities.showSnackBar(scaffoldKey,message: "Fullname is required");
    }
  }


}
