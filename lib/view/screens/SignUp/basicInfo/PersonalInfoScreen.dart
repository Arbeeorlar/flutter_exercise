

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_exercise/view/screens/SignUp/basicInfo/PinValidationScreen.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';
import 'package:flutter_exercise/view/widgets/BackgroundView.dart';
import 'package:flutter_exercise/view/widgets/DropDownFormFieldSP.dart';
import 'package:flutter_exercise/view/widgets/PoDateField.dart';

import '../../../Util.dart';

class PersonalInfoScreen extends StatefulWidget {
  @override
  _PersonalInfoScreenState createState() => _PersonalInfoScreenState();
}

class _PersonalInfoScreenState extends State<PersonalInfoScreen> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController fullNameController = TextEditingController();


  String dataFormat = 'yyyy-MMMM-dd';
  final DateTime previous18Years = DateTime(
      DateTime.now().year - 18,
      DateTime.now().month,
      DateTime.now().subtract(Duration(days: 1)).day); //.add(Duration());

  DateTime selectedDate ;

  String gender ;
  List<String> genderList = ["Male","Gender"] ;
  List<String> stateList = ["Lagos","Oyo","Kano","Kaduna"] ;
  List<String> cityList = ["Ikorodu","Abuleegbe","Kano","Kaduna"] ;
  String state;
  String city;


  void moveToSecondStep() async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => PinValidationScreen()));
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        BackgroundView(),
        SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            key: scaffoldKey,
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: Container(
              child: Form(
                key: formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30,),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 30,right: 30),
                            decoration: BoxDecoration(
                              color: AppColors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                SizedBox(height: 20.0),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(left: 30, right: 30),
                                  child: Text(
                                    "Basic Informations",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.black),
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 30, right: 30),
                                  child: TextFormField(
                                    controller: fullNameController,
                                    showCursor: true,
                                    keyboardType:
                                    TextInputType.text,
                                    textInputAction: TextInputAction.next,
                                    autocorrect: false,
                                    textAlign: TextAlign.left,
                                    decoration: InputDecoration(
                                      enabledBorder: new UnderlineInputBorder(
                                          borderSide: new BorderSide(color : AppColors.black)
                                      ),
                                      labelText: "Full Name",
                                      labelStyle: TextStyle(
                                          fontSize: 10.0,
                                          color: AppColors.black),
                                    ),
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14.0,
                                        color: AppColors.black),
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(50)
                                    ],
                                    onChanged: (text) {},
                                    validator: (String arg) {
                                      if (arg.length < 3)
                                        return 'Fullname  is required';
                                      else
                                        return null;
                                    },
                                    onSaved: (String val) {},
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                  width: double.infinity,
                                  margin:
                                  EdgeInsets.only(left: 30, right: 30),
                                  child: PoDateField(
                                    null,
                                    previous18Years,
                                    previous18Years, // bloc.uiModelNoBvnRegistration.dob,
                                  //  selectedDate: bloc.uiModelNoBvnRegistration.dob,
                                    title: "Select Date Of Birth",
                                    onDateSelected: (dateSelected) {

                                    },
                                    // error: (bloc.uiModelNoBvnRegistration.validated &&
                                    //     bloc.uiModelNoBvnRegistration.dob == null),
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                 margin : EdgeInsets.only(left: 30, right: 30),
                                  child: DropDownFormFieldSP<String>(
                                    hint: 'Select an Option',
                                    items: genderList,
                                    onItemSelected: (value) {
                                      setState(() {
                                        gender = value ;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                  margin : EdgeInsets.only(left: 30, right: 30),
                                  child: DropDownFormFieldSP<String>(
                                    hint: 'Select State',
                                    items: stateList,
                                    onItemSelected: (value) {
                                      setState(() {
                                        state = value ;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 30.0),
                                Container(
                                  margin : EdgeInsets.only(left: 30, right: 30),
                                  child: DropDownFormFieldSP<String>(
                                    hint: 'Select City',
                                    items: cityList,
                                    onItemSelected: (value) {
                                      setState(() {
                                        city = value ;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 30.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 45, right: 45),
                                  width: double.infinity,
                                  height: 50,
                                  child: FlatButton(
                                    onPressed: validate,
                                    child:  Text(
                                      "Continue",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    color: AppColors.primaryLight,
                                    textColor: AppColors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 35.0),

                              ],
                            ),
                          ),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
  void validate() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      moveToSecondStep();
    } else {
      Utilities.showSnackBar(scaffoldKey,message: "Fullname is required");
    }
  }

}
