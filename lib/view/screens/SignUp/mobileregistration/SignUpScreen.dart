
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_exercise/view/Util.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';
import 'package:flutter_exercise/view/widgets/BackgroundView.dart';

import 'OTPScreen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  var phoneCode = '+234';
  TextEditingController phoneController = TextEditingController();
  FocusNode phoneFocusNode;

  void moveToOTP(String completePhone) async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => OTPScreen(phoneNumber: completePhone,)));
  }

  @override
  void initState() {
    super.initState();

    phoneFocusNode = new FocusNode();
    phoneFocusNode.addListener(() {
      if (phoneFocusNode.hasFocus) {
        if(phoneController.text.substring(1) == "0"){
          setState(() {
            phoneController.text = phoneController.text.substring(1,phoneController.text.length);
          });
        }

      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        BackgroundView(),
        SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            key: scaffoldKey,
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: Container(
              child: Form(
                key: formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30,),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 30,right: 30),
                            decoration: BoxDecoration(
                              color: AppColors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                SizedBox(height: 20.0),
                                Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(left: 16, right: 30),
                                  child: Text(
                                    "Enter your Mobile Number",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.black),
                                  ),
                                ),
                                SizedBox(height: 50.0),
                                Row(
                                  children: [
                                    Container(
                                      margin:   const EdgeInsets.only( left: 25.0),
                                      height: 50.0,
                                      decoration: new BoxDecoration(
                                          color: AppColors.buttonLight.withOpacity(0.15),
                                          border: Border.all(
                                              color: AppColors.primaryLight),
                                          borderRadius:
                                          BorderRadius.circular(5.0)),
                                      child: CountryCodePicker(
                                        onChanged: (countryCode) {
                                          phoneCode = countryCode.toString();
                                          print("New Country selected: " +
                                              countryCode.toString());
                                        },
                                        dialogBackgroundColor: AppColors.primary,
                                        dialogTextStyle: TextStyle(color: AppColors.white),
                                        // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                        initialSelection: 'NG',
                                        favorite: ['+234', 'US'],
                                        // optional. Shows only country name and flag
                                        showCountryOnly: false,
                                        // optional. Shows only country name and flag when popup is closed.
                                        showOnlyCountryWhenClosed: false,
                                        // optional. aligns the flag and the Text left
                                        alignLeft: false,
                                        //itemBuilder: _buildDropdownItem,
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin:
                                        EdgeInsets.only(left: 10, right: 30),
                                        child: TextFormField(
                                          focusNode: phoneFocusNode,
                                          controller: phoneController,
                                          showCursor: true,
                                          keyboardType:
                                          TextInputType.phone,
                                          textInputAction: TextInputAction.next,
                                          autocorrect: false,
                                          textAlign: TextAlign.left,
                                          decoration: InputDecoration(
                                            enabledBorder: new UnderlineInputBorder(
                                                borderSide: new BorderSide(color: AppColors.black)
                                            ),
                                            labelText: "Enter your phone number",
                                            labelStyle: TextStyle(
                                                fontSize: 10.0,
                                                color: AppColors.black),
                                          ),
                                          style: TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 14.0,
                                              color: AppColors.black),
                                          inputFormatters: [
                                            LengthLimitingTextInputFormatter(20)
                                          ],
                                          onChanged: (text) {

                                          },
                                          validator: (String arg) {
                                            if (arg.length < 10)
                                              return 'Phone Number  is required';
                                            else
                                              return null;
                                          },
                                          onSaved: (String val) {},
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.0),

                                SizedBox(height: 20.0),

                                SizedBox(height: 30.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 45, right: 45),
                                  width: double.infinity,
                                  height: 50,
                                  child: FlatButton(
                                    onPressed: validateAndMove,
                                    child:  Text(
                                      "CONTINUE",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    color: AppColors.primaryLight,
                                    textColor: AppColors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 35.0),

                              ],
                            ),
                          ),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }


  void validateAndMove() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      String completePhone = "$phoneCode${phoneController.text}" ;

      moveToOTP(completePhone);
    } else {
     Utilities.showSnackBar(scaffoldKey,message: "Invalid Phone Number");
    }
  }


}
