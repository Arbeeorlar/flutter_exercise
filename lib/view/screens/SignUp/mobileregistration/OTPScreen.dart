


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_exercise/view/screens/SignUp/basicInfo/PersonalInfoScreen.dart';
import 'package:flutter_exercise/view/themes/AppColors.dart';
import 'package:flutter_exercise/view/widgets/BackgroundView.dart';
import 'package:flutter_exercise/view/widgets/PinView.dart';

import '../../../Util.dart';

class OTPScreen extends StatefulWidget {

  final String phoneNumber ;

  const OTPScreen({this.phoneNumber});


  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {


  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController pinViewController = TextEditingController();


  void moveToSecondStep() async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => PersonalInfoScreen()));
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        BackgroundView(),
        SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            key: scaffoldKey,
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: Container(
              child: Form(
                key: formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30,),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 30,right: 30),
                            decoration: BoxDecoration(
                              color: AppColors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                SizedBox(height: 30.0),
                                Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(left: 16, right: 30),
                                  child: Text(
                                    "Enter OTP sent to Mobile Number \n${widget.phoneNumber}",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.black),
                                  ),
                                ),

                                SizedBox(height: 50.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 30, right: 30),
                                  child: TextFormField(
                                    controller: pinViewController,
                                    showCursor: true,
                                    keyboardType:
                                    TextInputType.number,
                                    textInputAction: TextInputAction.next,
                                    autocorrect: false,
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                      enabledBorder: new UnderlineInputBorder(
                                          borderSide: new BorderSide(color: AppColors.black)
                                      ),
                                      labelText: "Input OTP",
                                      labelStyle: TextStyle(
                                          fontSize: 10.0,
                                          color: AppColors.black),
                                    ),
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14.0,
                                        color: AppColors.black),
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(6)
                                    ],
                                    onChanged: (text) {

                                    },
                                    validator: (String arg) {
                                      if (arg.length < 6)
                                        return 'Invalid OTP';
                                      else
                                        return null;
                                    },
                                    onSaved: (String val) {},
                                  ),
                                ),
                                SizedBox(height: 20.0),

                                SizedBox(height: 30.0),
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 45, right: 45),
                                  width: double.infinity,
                                  height: 50,
                                  child: FlatButton(
                                    onPressed: validateOTP,
                                    child:  Text(
                                      "Continue",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    color: AppColors.primaryLight,
                                    textColor: AppColors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(left: 16, right: 30),
                                  child: Text(
                                    "RESEND OTP",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.black),
                                  ),
                                ),
                                SizedBox(height: 35.0),

                              ],
                            ),
                          ),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void validateOTP() {
    final FormState form = formKey.currentState;
    if (form.validate()) {

      moveToSecondStep();

    } else {
      Utilities.showSnackBar(scaffoldKey,message: "Invalid Phone Number");
    }
  }
}
