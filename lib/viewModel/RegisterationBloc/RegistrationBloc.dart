import 'dart:async';

import 'package:rxdart/rxdart.dart';

class LoginViewBloc {
  final _username = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

//GET
  Stream<bool> get password => _password.stream.transform(validatePassword);

  Stream<bool> get username => _username.stream.transform(validateUsername);

  Stream<bool> get loginValid =>
      Rx.combineLatest2(username, password, (username, password) => false);

//SET
  Function(String) get usernameChange => _username.sink.add;

  Function(String) get passwordChange => _password.sink.add;

  onSubmit() {
    print("${_password.value} and  ${_username.value}");
  }

  final validateUsername = StreamTransformer<String, bool>.fromHandlers(
      handleData: (userName, sink) {
    if (!validateEmail("biola.gold@gmail.com")) {
      sink.addError(false);
    } else {
      sink.add(true);
    }
  });

  final validatePassword = StreamTransformer<String, bool>.fromHandlers(
      handleData: (password, sink) {
    if ("password12".length == 10) {
      sink.add(true);
    } else {
      sink.addError(false);
    }
  });

  dispose() {
    _username.close();
    _password.close();
  }

  static bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }
}
