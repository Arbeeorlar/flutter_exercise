class RegistrationModel {
  String phoneNumber;
  bool validateOTP;

  String fullName;
  String dob;
  String gender;
  String state;
  String city;
  String pin;
  String firstSecurityQuestion;
  String firstSecurityAnswer;
  String secondSecurityQuestion;
  String secondSecurityAnswer;

  RegistrationModel(
      {this.phoneNumber,
      this.validateOTP,
      this.fullName,
      this.dob,
      this.gender,
      this.state,
      this.city,
      this.pin,
      this.firstSecurityAnswer,
      this.firstSecurityQuestion,
      this.secondSecurityAnswer,
      this.secondSecurityQuestion});
}
